using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float speed;
    public Vector3 direction;

    void Update()
    {
        float step = speed * Time.deltaTime; 
        gameObject.transform.Rotate(direction * step);
    }
}
