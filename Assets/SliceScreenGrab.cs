using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SliceScreenGrab : MonoBehaviour
{
    // Grabbing the Render Texture and Scene Objects
    public GameObject slicedImages;
    public GameObject objects1;
    public GameObject objects2;
    
    private Texture2D _texture;
    private Camera _mainCamera;
    private bool _chopping;

    public void Start()
    {
        _mainCamera = Camera.main;

        if (!_mainCamera) return;
        
        // Move the UI menu so the bottom left corner is at position 0,0,0. Makes it a little easier to position Game Objects 
        float aspectRatio = _mainCamera.aspect;
        float camSize = _mainCamera.orthographicSize;
        float camPositionX = aspectRatio * camSize;
        _mainCamera.transform.position = new Vector3(camPositionX, camSize, -1);
    }

    public void FixedUpdate()
    {
        if(_texture && _chopping)
        {
            _chopping = false;
            SliceImage();
            
            bool scene1Active = !objects1.activeInHierarchy;
            objects1.SetActive(scene1Active);

            bool scene2Active = !objects2.activeInHierarchy;
            objects2.SetActive(scene2Active);

        }

        if (slicedImages.transform.childCount > 0)
        {
            // I should really destroy and recreate the parent because so that it's not endlessly falling
            slicedImages.transform.Translate(Vector3.down * (Time.deltaTime * 20));
        }
    }

    public void MeltScreen()
    {
        _chopping = true;
        
        foreach (Transform t in slicedImages.transform)
        {
          Destroy(t.gameObject);
        }

        StartCoroutine(GrabScreen());
    }

    private void SliceImage()
    {
        for (int i = 0; i < 80; i++)
        {
            Sprite strip = Sprite.Create(_texture, new Rect(i * 24, 0, 24, Screen.height), new Vector2(0.5f, 0.5f), 100);

            Image newStrip = new GameObject().AddComponent<Image>();
            newStrip.sprite = strip;
            newStrip.SetNativeSize();
            newStrip.transform.SetParent(transform.parent, false);
            
            newStrip.GetComponent<RectTransform>().anchoredPosition = new Vector2(i * 24 - Screen.width / 2 + 50, 
                Random.Range(25, 200)); ;
            
            newStrip.transform.SetParent(slicedImages.transform);
        }
    }

    private IEnumerator GrabScreen()
    {
        yield return new WaitForEndOfFrame();
        
        _mainCamera.targetTexture = RenderTexture.GetTemporary(Screen.width, Screen.height, 16);
        RenderTexture renderTexture = _mainCamera.targetTexture;

        _texture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGB24, false);
        _texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0, false);
        _texture.Apply();

        RenderTexture.ReleaseTemporary(renderTexture);
        _mainCamera.targetTexture = null;

        StopAllCoroutines();
    }
}
   
